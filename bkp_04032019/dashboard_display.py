def dashboard_display():
    try:
        from db_connect import db_conn
        conn = db_conn()
        cur = conn.cursor()
        cur.execute("select sum(enc)-sum(exc),sum(ena)-sum(exa) from v_inventory")
        total_inv_dash = cur.fetchall()
        cur.execute("select sum(quantity),sum(amount) from entry_details where cast(entered_date as date) = current_date")
        todays_entry_dash = cur.fetchall()
        cur.execute("select sum(quantity),sum(amount) from exit_details where cast(exit_date as date) = current_date and exit_type='WS'")
        todays_ws_dash = cur.fetchall()
        cur.execute("select sum(quantity),sum(amount) from exit_details where cast(exit_date as date) = current_date and exit_type='CS'")
        todays_cs_dash = cur.fetchall()
        conn.close()
        return(total_inv_dash,todays_entry_dash,todays_ws_dash,todays_cs_dash)
    except Exception as e:
        print(e)
        return(e)
