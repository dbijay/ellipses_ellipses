from flask import request, Flask, render_template, session, redirect, jsonify
import datetime
import hashlib
#from config_file import *
from db_connect import db_conn
from list_part_numbers import list_parts,list_parts_available
from dashboard_display import dashboard_display
from user_audit import user_audit
app = Flask(__name__)
app.secret_key = 'nchl'

@app.route('/')
def root_page():
    try:
        if 'username' not in session:
            return render_template('login_page.html')
        elif 'username' in session:
            user_audit(session['username'],'NO_TABLE','GET_ROOT_PAGE',request)
            return render_template('dashboard.html',user = session['username'],cards = dashboard_display())
    except Exception as e:
        print('Error Occured',e)
        return render_template('login_page.html',msg = 'Something Happened! Please Contact Administrator!')


@app.route('/login', methods = ['POST'])
def homepage():
    try:
        if 'username' not in session:
            try:
                if request.form['username'] is not None:
                    username = request.form['username']
                    password = request.form['password']
                    conn = db_conn()
                    cur = conn.cursor()
                    qry = "select password from user_table where username = (%s)"
                    cur.execute(qry,(username,))
                    password_e=(cur.fetchone()[0])
                    if hashlib.sha256(password.encode()).hexdigest() == password_e:
                        session['username'] = username
                        user_audit(username,'session_table','LOGIN',request)
                        return render_template('main_page.html', user=username)
                    else:
                        return render_template('login_page.html',msg = "Login Failed. Please try again!!")
            except Exception as e:
                print(e)
                return render_template('login_page.html', msg="Login Failed. Please try again!!")
        elif 'username' in session:
            username = session['username']
            return render_template('main_page.html', user = username)
    except Exception as e:
        print(e)
        return render_template('login_page.html', msg="Something unexpected happened. Please try again !!")

@app.route('/dashboard')
def dashboard():
    if 'username' in session:
        print(list_parts())
        return render_template('dashboard.html',user = session['username'],cards = dashboard_display())
    elif 'username' not in session:
        return render_template('login_page.html',msg = 'Please Login First!!')

@app.route('/manual_entry')
def manual_entry():
    if 'username' in session:
        print(list_parts())
        return render_template('manual_entry.html',user = session['username'], item_details = list_parts())
    elif 'username' not in session:
        return render_template('login_page.html',msg = 'Please Login First!!')


@app.route('/manual_entry_submit', methods = ['POST','GET'])
def manual_entry_submit():
    try:
        if request.method == 'POST':
            if 'username' in session:
                from post_values_handler import manual_entry_handler
                res = manual_entry_handler(session['username'],request.form)
                if res == 'EMPTY_FIELDS':
                    return render_template('manual_entry.html',
                                                   notification_msg="Empty Fields not allowed.",user=session['username'], item_details = list_parts())
                elif res == 'SUCCESS':
                    return render_template('manual_entry.html', notification_msg="Item details Entered Successfully.",
                                       user=session['username'], item_details = list_parts())
            elif 'username' not in session:
                return render_template('login_page.html',msg = 'Please Login First!!')
    except Exception as e:
        print(e)
        return render_template('manual_entry.html', notification_msg="Entry Failed!! Exception Occured !!",
                               user=session['username'], item_details=list_parts())


@app.route('/csv_upload')
def csv_upload():
    if 'username' in session:
        return render_template('csv_upload.html',user = session['username'])
    elif 'username' not in session:
        return render_template('login_page.html',msg = 'Please Login First!!')

@app.route('/csv_file_submit', methods = ['POST','GET'])
def csv_file_submit():
    try:
        if request.method == 'POST':
            if 'username' in session:
                from post_values_handler import csv_upload_handler
                res = csv_upload_handler(session['username'],request.form,request.files)
                if res == 'EMPTY_FIELDS':
                    return render_template('csv_upload.html',
                                                   notification_msg="Empty Fields not allowed.",user=session['username'])
                elif res == 'NO_FILE':
                        return render_template('csv_upload.html',
                                               notification_msg="No Valid File.", user=session['username'])
                elif res == 'INVALID_HEADER':
                        return render_template('csv_upload.html',
                                               notification_msg="Invalid Headers.", user=session['username'])
                elif res == 'INVALID_ROWS':
                        return render_template('csv_upload.html',
                                               notification_msg="Invalid Row/s.", user=session['username'])
                elif res == 'SUCCESS':
                    return render_template('csv_upload.html', notification_msg="Item details Entered Successfully.",
                                       user=session['username'])
                return render_template('csv_upload.html',notification_msg="File has been uploaded.")
            elif 'username' not in session:
                return render_template('login_page.html',msg = 'Please Login First!!')
    except Exception as e:
        print(e)
        return render_template('csv_upload.html', notification_msg="Entry Failed!!!", user = session['username'])

@app.route('/entry_report')
def entry_report():
    if 'username' in session:
        return render_template('entry_report.html',user = session['username'])
    elif 'username' not in session:
        return render_template('login_page.html',msg = 'Please Login First!!')

@app.route('/entry_report_submit', methods = ['POST','GET'])
def entry_report_submit():
    try:
        if request.method == 'POST':
            if 'username' in session:
                entered_date_from = (request.form['entered_date_from']) if (request.form['entered_date_from']) else "1111-11-11"
                entered_date_to = (request.form['entered_date_to']) if (request.form['entered_date_to']) else "3000-12-12"
                bill_date3 = (request.form['bill_date3']) if (request.form['bill_date3']) else "%"
                bill_number3 = (request.form['bill_number3']) if (request.form['bill_number3']) else "%"
                entered_by = (request.form['entered_by']) if (request.form['entered_by']) else "%"
                conn = db_conn()
                cur = conn.cursor()
                qry = "select * from v_entry_report where cast(entered_date as date) between (%s) and (%s) and cast(bill_date as text) like (%s) and bill_number like (%s) and entered_by like (%s)"
                print(qry,(entered_date_from,entered_date_to,bill_date3,bill_number3,entered_by))
                cur.execute(qry,(entered_date_from,entered_date_to,bill_date3,bill_number3,entered_by))
                result = (cur.fetchall())
                conn.close()
                return render_template('entry_report.html',entry_report=result,SN=1)
            elif 'username' not in session:
                return render_template('login_page.html',msg = 'Please Login First!!')
    except Exception as e:
        print(e)
        return render_template('entry_report.html', user=session['username'])

@app.route('/exit_report')
def exit_report():
    if 'username' in session:
        return render_template('exit_report.html',user = session['username'])
    elif 'username' not in session:
        return render_template('login_page.html',msg = 'Please Login First!!')

@app.route('/exit_report_submit', methods = ['POST','GET'])
def exit_report_submit():
    try:
        if request.method == 'POST':
            if 'username' in session:
                print(request.form)
                exit_date_from = (request.form['exit_date_from']) if (request.form['exit_date_from']) else "1111-11-11"
                exit_date_to = (request.form['exit_date_to']) if (request.form['exit_date_to']) else "3000-12-12"
                bill_date3 = (request.form['bill_date3']) if (request.form['bill_date3']) else "%"
                bill_number3 = (request.form['bill_number3']) if (request.form['bill_number3']) else "%"
                exit_by = (request.form['exit_by']) if (request.form['exit_by']) else "%"
                print(exit_date_from)
                print(exit_date_to)
                print(bill_date3)
                print(bill_number3)
                print(exit_by)
                conn = db_conn()
                cur = conn.cursor()
                qry = "select * from v_exit_report where cast(exit_date as date) between (%s) and (%s) and cast(bill_date as text) like (%s) and bill_number like (%s) and exit_by like (%s)"
                print(qry,(exit_date_from,exit_date_to,bill_date3,bill_number3,exit_by))
                cur.execute(qry,(exit_date_from,exit_date_to,bill_date3,bill_number3,exit_by))
                result = (cur.fetchall())
                conn.close()
                return render_template('exit_report.html',exit_report=result)
            elif 'username' not in session:
                return render_template('login_page.html',msg = 'Please Login First!!')
    except Exception as e:
        print(e)
        return render_template('exit_report.html', user=session['username'])


@app.route('/total_inventory')
def total_inventory():
    if 'username' in session:
        return render_template('total_inventory.html',user = session['username'], item_details=list_parts_available())
    elif 'username' not in session:
        return render_template('login_page.html',msg = 'Please Login First!!')

@app.route('/total_inventory_submit', methods= ['POST','GET'])
def total_inventory_submit():
    try:
        if request.method == 'POST':
            if 'username' in session:
                part_number8 = request.form['part_number8'][0:12]
                part_number88 = part_number8 if part_number8 else '%'
                print(part_number88)
                conn = db_conn()
                cur = conn.cursor()
                cur.execute("select * from v_inventory where part_number like %(part_number88)s",{'part_number88': str(part_number88)})
                result88 = (cur.fetchall())
                conn.close()
                print(result88)
                return render_template('total_inventory.html',user = session['username'], total_inventory = result88, item_details=list_parts_available())
            elif 'username' not in session:
                return render_template('login_page.html',msg = 'Please Login First!!')
    except Exception as e:
        return render_template('total_inventory.html', user=session['username'], item_details=list_parts_available())

@app.route('/workshop_exit')
def workshop_exit():
    if 'username' in session:
        return render_template('new_ws_exit.html',user = session['username'], item_details = list_parts_available())
    elif 'username' not in session:
        return render_template('login_page.html',msg = 'Please Login First!!')

@app.route('/workshop_exit_form_submit', methods=['POST','GET'])
def workshop_exit_submit():
    try:
        if request.method == 'POST':
            if 'username' in session:
                exit_type = 'WS'
                from post_values_handler import workshop_exit_handler
                res = workshop_exit_handler(session['username'], request.form,exit_type)
                if res == 'EMPTY_FIELDS':
                    return render_template('new_ws_exit.html',
                                           notification_msg="Empty Fields not allowed.", user=session['username'],
                                           item_details=list_parts_available())
                elif res == 'SUCCESS':
                    return render_template('new_ws_exit.html', notification_msg="Items Exited Successfully.",
                                           user=session['username'], item_details=list_parts_available())
            elif 'username' not in session:
                return render_template('login_page.html', msg='Please Login First!!')
    except Exception as e:
            print(e)
            return render_template('new_ws_exit.html', notification_msg="Exit Failed!! Exception Occured !!",
                                   user=session['username'], item_details=list_parts_available())


@app.route('/counter_sales')
def counter_sales():
    if 'username' in session:
        return render_template('counter_sales.html',user = session['username'], item_details = list_parts_available())
    elif 'username' not in session:
        return render_template('login_page.html',msg = 'Please Login First!!')

@app.route('/counter_sales_submit', methods=['POST','GET'])
def counter_sales_submit():
    try:
        if request.method == 'POST':
            if 'username' in session:
                exit_type = 'CS'
                from post_values_handler import workshop_exit_handler
                res = workshop_exit_handler(session['username'], request.form,exit_type)
                if res == 'EMPTY_FIELDS':
                    return render_template('counter_sales.html',
                                           notification_msg="Empty Fields not allowed.", user=session['username'],
                                           item_details=list_parts_available())
                elif res == 'SUCCESS':
                    return render_template('counter_sales.html', notification_msg="Items Exited Successfully.",
                                           user=session['username'], item_details=list_parts_available())
            elif 'username' not in session:
                return render_template('login_page.html', msg='Please Login First!!')
    except Exception as e:
            print(e)
            return render_template('counter_sales.html', notification_msg="Exit Failed!! Exception Occured !!",
                                   user=session['username'], item_details=list_parts_available())

@app.route('/damaged_items')
def damaged_items():
    if 'username' in session:
        return render_template('damaged_items.html',user = session['username'], item_details = list_parts_available())
    elif 'username' not in session:
        return render_template('login_page.html',msg = 'Please Login First!!')

@app.route('/damaged_items_submit', methods=['POST','GET'])
def damaged_items_submit():
    try:
        if request.method == 'POST':
            if 'username' in session:
                exit_type = 'DI'
                from post_values_handler import workshop_exit_handler
                res = workshop_exit_handler(session['username'], request.form,exit_type)
                if res == 'EMPTY_FIELDS':
                    return render_template('damaged_items.html',
                                           notification_msg="Empty Fields not allowed.", user=session['username'],
                                           item_details=list_parts_available())
                elif res == 'SUCCESS':
                    return render_template('damaged_items.html', notification_msg="Items Exited Successfully.",
                                           user=session['username'], item_details=list_parts_available())
            elif 'username' not in session:
                return render_template('login_page.html', msg='Please Login First!!')
    except Exception as e:
            print(e)
            return render_template('damaged_items.html', notification_msg="Exit Failed!! Exception Occured !!",
                                   user=session['username'], item_details=list_parts_available())

@app.route('/items_lookup', methods=['POST','GET'])
def items_lookup():
    try:
        if request.method == 'GET':
            if 'username' in session:
                conn = db_conn()
                cur = conn.cursor()
                qry = "select * from item_details_table where part_number in (select distinct(part_number) from entry_details)"
                cur.execute(qry)
                result10 = (cur.fetchall())
                conn.close()
                return render_template('items_lookup.html', items_lookup=result10, user = session['username'])
            elif 'username' not in session:
                return render_template('login_page.html', msg='Please Login First!!', user = session['username'])
    except Exception as e:
        print(e)
        return render_template('items_lookup.html',user = session['username'])

@app.route('/modal_display_entry_reports')
def modal_display_entry_reports():
        try:
            entrynumber = request.args.get('entrynumber',0,type=str)
            print(entrynumber)
            conn = db_conn()
            cur = conn.cursor()
            cur.execute("select part_number,part_name,cast(quantity as text),cast(amount as text),bill_date,entered_by,remarks,purchased_from from entry_details where entry_number=%(entrynumber)s",{'entrynumber':str(entrynumber)})
            result = (cur.fetchall())
            print((result))
            conn.close()
            return jsonify(modal_results = result)
        except Exception as e:
            return jsonify(modal_results = str(e))

@app.route('/modal_display_exit_reports')
def modal_display_exit_reports():
        try:
            exitnumber = request.args.get('exitnumber',0,type=str)
            print(exitnumber)
            conn = db_conn()
            cur = conn.cursor()
            cur.execute("select part_number,part_name,cast(quantity as text),cast(amount as text),bill_date,exit_by,remarks,sold_to from exit_details where exit_number=%(exitnumber)s",{'exitnumber':str(exitnumber)})
            result = (cur.fetchall())
            print((result))
            conn.close()
            return jsonify(modal_results = result)
        except Exception as e:
            return jsonify(modal_results = str(e))

@app.route('/modal_display_inventory_reports')
def modal_display_inventory_reports():
        try:
            part_number_inv = request.args.get('part_number_inv',0,type=str)
            print(part_number_inv)
            conn = db_conn()
            cur = conn.cursor()
            cur.execute("select part_number,part_name,cast(quantity as text),remarks,entered_by,left(entered_date,10),entry_number,"
                        "entry_type,cast(amount as text),cast(bill_date as text),bill_number,purchased_from from entry_details where part_number=%(partnumberinv)s order by id desc",{'partnumberinv':str(part_number_inv)})
            result1 = (cur.fetchall())
            cur.execute("select part_number,part_name,cast(quantity as text),remarks,exit_by,left(exit_date,10),exit_number,"
                        "exit_type,cast(amount as text),cast(bill_date as text),bill_number,sold_to from exit_details where part_number=%(partnumberinv)s order by id desc",{'partnumberinv':str(part_number_inv)})
            result2 = (cur.fetchall())
            print((result1))
            print(result2)
            conn.close()
            return jsonify(modal_results1 = result1, modal_results2 = result2)
        except Exception as e:
            return jsonify(modal_results = str(e))

@app.route('/logout')
def log_out():
    session.pop('username', None)
    return render_template('login_page.html')

if  __name__ == "__main__":
    app.run(host = '0.0.0.0')
