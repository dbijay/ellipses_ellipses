def manual_entry_handler(user,request_form):
    import datetime
    from db_connect import db_conn
    conn = db_conn()
    print(request_form)
    i = 2
    j = i
    entry_number = 'EN_%s' % datetime.datetime.now().strftime("%y%m%d%H%M%S")
    bill_number = request_form['bill_number1']
    bill_date = request_form['bill_date1']
    remarks = request_form['remarks1']
    purchased_from = request_form['purchased_from1']
    if bill_number == "" or bill_date == "" or purchased_from == "":
        return('EMPTY_FIELDS')

    print(bill_number)
    print(entry_number)
    print(user)
    print(bill_date)
    print(remarks)
    print(purchased_from)
    print(len(request_form))
    print((len(request_form)-4)/4)
    while i < (len(request_form)-4)/4 + 2:
        part_number = 'part_number' + str(j)
        quantity = 'quantity' + str(j)
        amount = 'amount' + str(j)
        if (part_number) in request_form:
            if request_form[part_number] == "" or request_form[quantity] == "" or request_form[amount] == "":
                return('EMPTY_FIELDS')

            cur = conn.cursor()
            qry = "insert into entry_details (entry_number,bill_number,purchased_from,part_number,quantity,entered_by,entered_date,entry_type,amount,remarks) values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(entry_number,bill_number,purchased_from,request_form[part_number], request_form[quantity],user,str(datetime.datetime.now()), 'MANUAL', request_form[amount],remarks)
            print(qry)
            cur.execute("insert into entry_details (entry_number,bill_number,purchased_from,part_number,quantity,entered_by,entered_date,entry_type,amount,remarks,bill_date) values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(entry_number,bill_number,purchased_from,request_form[part_number], request_form[quantity],user,str(datetime.datetime.now()), 'MANUAL', request_form[amount],remarks,bill_date))
            i += 1
        j += 1
    conn.commit()
    conn.close()
    return('SUCCESS')


def workshop_exit_handler(user,request_form,exit_type):
    import datetime
    from db_connect import db_conn
    conn = db_conn()
    print(request_form)
    i = 2
    j = i
    exit_number = 'EX_%s' % datetime.datetime.now().strftime("%y%m%d%H%M%S")
    bill_number = request_form['bill_number1']
    bill_date = request_form['bill_date1']
    remarks = request_form['remarks1']
    sold_to = request_form['sold_to']
    if bill_number == "" or bill_date == "" or sold_to == "":
        return('EMPTY_FIELDS')
    print(exit_type)
    print(bill_number)
    print(exit_number)
    print(user)
    print(bill_date)
    print(remarks)
    print(sold_to)
    print(len(request_form))
    print((len(request_form)-4)/4)
    while i < (len(request_form)-4)/4 + 2:

        part_number = 'part_number' + str(j)
        quantity = 'quantity' + str(j)
        amount = 'amount' + str(j)
        print(part_number)
        print(quantity)
        print(amount)
        if (part_number) in request_form:
            if request_form[part_number] == "" or request_form[quantity] == "" or request_form[amount] == "":
                return('EMPTY_FIELDS')
            cur = conn.cursor()
            qry = "insert into exit_details (exit_number,bill_number,bill_date,sold_to,part_number,quantity,exit_by,exit_date,exit_type,amount,remarks) values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(exit_number,bill_number,bill_date,sold_to,request_form[part_number], request_form[quantity],user,str(datetime.datetime.now()), exit_type, request_form[amount],remarks)
            print(qry)
            cur.execute("insert into exit_details (exit_number,bill_number,bill_date,sold_to,part_number,quantity,exit_by,exit_date,exit_type,amount,remarks) values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(exit_number,bill_number,bill_date,sold_to,request_form[part_number], request_form[quantity],user,str(datetime.datetime.now()), exit_type, request_form[amount],remarks))
            i += 1
        j += 1

    conn.commit()
    conn.close()
    return('SUCCESS')

def csv_upload_handler(user,request_form,request_files):
    import datetime
    from db_connect import db_conn
    entry_number = 'EN_%s' % datetime.datetime.now().strftime("%y%m%d%H%M%S")
    bill_number = request_form['bill_number1']
    bill_date = request_form['bill_date1']
    remarks2 = request_form['remarks2']
    purchased_from = request_form['purchased_from1']
    if bill_number == "" or bill_date == "" or purchased_from == "":
        return('EMPTY_FIELDS')

    file = request_files['csv_file']
    if not file:
        return "NO_FILE"
    file_contents = file.stream.read().decode("utf-8")
    file_contents = file_contents.splitlines()
    if file_contents[0].strip('"').split('","') != ['\ufeff"Item Group', 'Item No', 'Item Name', 'Bin Code',
                                                    'Order Quantity', 'Picking Quantity', 'Superseeding Part No',
                                                    'Superseeding Quantity', 'Approved Quantity', 'Approved Percentage',
                                                    'Box No', 'Remarks', 'Is Canceled', 'Remarks']:
        return "INVALID_HEADER"
    del (file_contents[0])
    for lines in file_contents:
        if len(lines.strip('"').split('","')) != 13:
            return "INVALID_ROWS"
        part_number2 = lines.strip('"').split('","')[1]
        quantity2 = lines.strip('"').split('","')[8]
        conn = db_conn()
        cur = conn.cursor()
        cur.execute(
            "insert into entry_details (entry_number,part_number,quantity,remarks,entered_by,entered_date,entry_type) values (%s,%s,%s,%s,%s,%s,%s)",
            (
                entry_number, part_number2, quantity2, remarks2, user, str(datetime.datetime.now()),
                'CSV'))
        conn.commit()
        conn.close()
    return "SUCCESS"