def dashboard_display(org_id):
    try:
        from db_connect import db_conn
        conn = db_conn()
        cur = conn.cursor()
        cur.execute("select sum(entry_quantity)-sum(exit_quantity),sum(entry_amount)-sum(exit_amount) from v_inventory where entry_org_id=%(org_id)s",{'org_id':str(org_id)})
        total_inv_dash = cur.fetchall()
        cur.execute("select sum(quantity),sum(amount) from entry_details where cast(entered_date as date) = current_date and org_id = %(org_id)s",{'org_id':str(org_id)})
        todays_entry_dash = cur.fetchall()
        cur.execute("select sum(quantity),sum(amount) from exit_details where cast(exit_date as date) = current_date and exit_type='WS' and org_id = %(org_id)s",{'org_id':str(org_id)})
        todays_ws_dash = cur.fetchall()
        cur.execute("select sum(quantity),sum(amount) from exit_details where cast(exit_date as date) = current_date and exit_type='CS' and org_id = %(org_id)s",{'org_id':str(org_id)})
        todays_cs_dash = cur.fetchall()
        conn.close()
        return(total_inv_dash,todays_entry_dash,todays_ws_dash,todays_cs_dash)
    except Exception as e:
        print(e)
        return(e)
print(dashboard_display('0001'))