import psycopg2

def db_conn():
    host = 'postgresql.ellipses.svc.cluster.local'
    port = '5432'
    database = 'inventory_mgmt'
    user = 'ellipses_admin'
    passw = 'P0$tgr3s'
    try:
        conn1 = psycopg2.connect(host=host,database=database,port=port, user=user, password=passw)
        print(conn1)
    except Exception as e:
        print("Error in connecting DB",e)
        conn1 = 'ERR'
    return(conn1)
db_conn()
