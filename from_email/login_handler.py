from flask import request, Flask, render_template, session, redirect
import datetime
import hashlib
from config_file import *
from db_connect import db_conn
from list_part_numbers import list_parts,list_parts_available

app = Flask(__name__)
app.secret_key = 'nchl'

@app.route('/')
def root_page():
    try:
        if 'username' not in session:
            return render_template('login_page.html')
        elif 'username' in session:
            return render_template('main_page.html',user = session['username'])
    except Exception as e:
        logging.info('Error Occured',e)

@app.route('/login', methods = ['POST'])
def homepage():
    if 'username' not in session:
        try:
            if request.form['username'] is not None:
                username = request.form['username']
                password = request.form['password']
                conn = db_conn()
                cur = conn.cursor()
                qry = "select password from user_table where username = (%s)"
                cur.execute(qry,(username,))
                password_e=(cur.fetchone()[0])
                if hashlib.sha256(password.encode()).hexdigest() == password_e:
                #if password_e == password:
                    session['username'] = username
                    return render_template('main_page.html', user=username)
                else:
                    return render_template('login_page.html',msg = "Login Failed. Please try again!!")
        except Exception as e:
            print(e)
            return render_template('login_page.html', msg="Login Failed. Please try again!!")
    else:
        username = session['username']
        return render_template('main_page.html', user = username)

@app.route('/manual_entry')
def manual_entry():
    if 'username' in session:
        return render_template('manual_entry_newest.html',user = session['username'], item_details = list_parts())
    elif 'username' not in session:
        return render_template('login_page.html',msg = 'Please Login First!!')


@app.route('/manual_entry_submit2', methods = ['POST','GET'])
def manual_entry_submit2():
    try:
        if request.method == 'POST':
            if 'username' in session:
                from post_values_handler import manual_entry_handler
                res = manual_entry_handler(session['username'],request.form)
                if res == 'EMPTY_FIELDS':
                    return render_template('manual_entry_newest.html',
                                                   notification_msg="Empty Fields not allowed.",user=session['username'], item_details = list_parts())
                elif res == 'SUCCESS':
                    return render_template('manual_entry_newest.html', notification_msg="Item details Entered Successfully.",
                                       user=session['username'], item_details = list_parts())
            elif 'username' not in session:
                return render_template('login_page.html',msg = 'Please Login First!!')
    except Exception as e:
        print(e)
        return render_template('manual_entry_newest.html', notification_msg="Entry Failed!! Exception Occured !!",
                               user=session['username'], item_details=list_parts())


@app.route('/csv_upload')
def csv_upload():
    if 'username' in session:
        return render_template('csv_upload.html',user = session['username'])
    elif 'username' not in session:
        return render_template('login_page.html',msg = 'Please Login First!!')

@app.route('/csv_file_submit', methods = ['POST','GET'])
def csv_file_submit():
    try:
        if request.method == 'POST':
            if 'username' in session:
                entry_number = 'ENTRY_%s' % datetime.datetime.now().strftime("%Y%m%d%H%M%S")
                file = request.files['csv_file']
                if not file:
                    return "No file"
                file_contents = file.stream.read().decode("utf-8")
                file_contents = file_contents.splitlines()
                if file_contents[0].strip('"').split('","') != ['\ufeff"Item Group', 'Item No', 'Item Name', 'Bin Code', 'Order Quantity', 'Picking Quantity', 'Superseeding Part No', 'Superseeding Quantity', 'Approved Quantity', 'Approved Percentage', 'Box No', 'Remarks', 'Is Canceled', 'Remarks']:
                    return render_template('csv_upload.html', notification_msg='Invalid Header!')
                del(file_contents[0])
                remarks2 = request.form['remarks2']
                user = session['username']
                from db_connect import db_conn
                for lines in file_contents:
                    if len(lines.strip('"').split('","')) != 13:
                        return render_template('csv_upload.html', notification_msg='Invalid number of items in some row!')
                    part_number2 =lines.strip('"').split('","')[1]
                    quantity2 = lines.strip('"').split('","')[8]
                    conn = db_conn()
                    cur = conn.cursor()
                    cur.execute(
                       "insert into entry_details (entry_number,part_number,quantity,remarks,entered_by,entered_date,entry_type) values (%s,%s,%s,%s,%s,%s,%s)",
                      (
                     entry_number, part_number2, quantity2, remarks2, user, str(datetime.datetime.now()),
                    'CSV'))
                    conn.commit()
                    conn.close()
                return render_template('csv_upload.html',notification_msg="File has been uploaded.")
            elif 'username' not in session:
                return render_template('login_page.html',msg = 'Please Login First!!')
    except Exception as e:
        print(e)
        return render_template('csv_upload.html', notification_msg="Entry Failed!!!", user = session['username'])

@app.route('/entry_report')
def entry_report():
    if 'username' in session:
        return render_template('entry_report.html',user = session['username'])
    elif 'username' not in session:
        return render_template('login_page.html',msg = 'Please Login First!!')

@app.route('/entry_report_submit', methods = ['POST','GET'])
def entry_report_submit():
    try:
        if request.method == 'POST':
            if 'username' in session:
                entered_date_from = (request.form['entered_date_from']) if (request.form['entered_date_from']) else "1111-11-11"
                entered_date_to = (request.form['entered_date_to']) if (request.form['entered_date_to']) else "3000-12-12"
                part_number3 = (request.form['part_number3']) if (request.form['part_number3']) else "%"
                quantity3 = (request.form['quantity3']) if (request.form['quantity3']) else "%"
                amount3 = (request.form['amount3']) if (request.form['amount3']) else "%"
                entered_by = (request.form['entered_by']) if (request.form['entered_by']) else "%"
                conn = db_conn()
                cur = conn.cursor()
                qry = "select entry_number,part_number,part_name,quantity,000 as amount,remarks,entry_type,left(entered_date,10),entered_by from entry_details where date_trunc('day',CAST(entered_date AS DATE)) between (%s) and (%s) and part_number like (%s) and cast(quantity as text) like (%s) and entered_by like (%s)"
                cur.execute(qry,(entered_date_from,entered_date_to,part_number3,quantity3,entered_by))
                result = (cur.fetchall())
                conn.close()
                return render_template('entry_report.html',entry_report=result)
            elif 'username' not in session:
                return render_template('login_page.html',msg = 'Please Login First!!')
    except Exception as e:
        print(e)
        return render_template('entry_report.html', user=session['username'])

@app.route('/exit_report')
def exit_report():
    if 'username' in session:
        return render_template('exit_report.html',user = session['username'])
    elif 'username' not in session:
        return render_template('login_page.html',msg = 'Please Login First!!')

@app.route('/exit_report_submit', methods = ['POST','GET'])
def exit_report_submit():
    try:
        if request.method == 'POST':
            if 'username' in session:
                exit_date_from7 = (request.form['exit_date_from7']) if (request.form['exit_date_from7']) else "1111-11-11"
                exit_date_to7 = (request.form['exit_date_to7']) if (request.form['exit_date_to7']) else "3000-12-12"
                part_number7 = (request.form['part_number7']) if (request.form['part_number7']) else "%"
                quantity7 = (request.form['quantity7']) if (request.form['quantity7']) else "%"
                amount7 = (request.form['amount7']) if (request.form['amount7']) else "%"
                exit_by = (request.form['exit_by']) if (request.form['exit_by']) else "%"
                conn = db_conn()
                cur = conn.cursor()
                qry = "select exit_number,part_number,part_name,quantity,amount,remarks,exit_type,left(exit_date,10),exit_by from exit_details where date_trunc('day',CAST(exit_date AS DATE)) between (%s) and (%s) and part_number like (%s) and cast(quantity as text) like (%s) and exit_by like (%s) and cast(amount as text) like (%s)"
                cur.execute(qry,(exit_date_from7,exit_date_to7,part_number7,quantity7,exit_by,amount7))
                result7 = (cur.fetchall())
                conn.close()
                return render_template('exit_report.html',exit_report=result7)
            elif 'username' not in session:
                return render_template('login_page.html',msg = 'Please Login First!!')
    except Exception as e:
        print(e)
        return render_template('exit_report.html', user=session['username'])


@app.route('/total_inventory')
def total_inventory():
    if 'username' in session:
        return render_template('view_inventory.html',user = session['username'])
    elif 'username' not in session:
        return render_template('login_page.html',msg = 'Please Login First!!')

@app.route('/total_inventory_submit', methods= ['POST','GET'])
def total_inventory_submit():
    try:
        if request.method == 'POST':
            if 'username' in session:
                conn = db_conn()
                cur = conn.cursor()
                qry = "select * from v_INVENTORY"
                cur.execute(qry)
                result8 = (cur.fetchall())
                conn.close()

                return render_template('view_inventory.html',user = session['username'], inv_report = result8)
            elif 'username' not in session:
                return render_template('login_page.html',msg = 'Please Login First!!')
    except Exception as e:
        return render_template('view_inventory.html', user=session['username'])

@app.route('/workshop_exit')
def workshop_exit():
    if 'username' in session:
        return render_template('new_ws_exit.html',user = session['username'], item_details = list_parts_available())
    elif 'username' not in session:
        return render_template('login_page.html',msg = 'Please Login First!!')

@app.route('/workshop_exit_submit', methods=['POST','GET'])
def workshop_exit_submit():
    try:
        if request.method == 'POST':
            if 'username' in session:
                exit_number = 'EX_%s' % datetime.datetime.now().strftime("%Y%m%d%H%M%S")
                workshop_name = request.form['work_shop_name4']
                part_number4 = request.form['part_number4']
                #part_name4 = request.form['part_name4']
                quantity4 = request.form['quantity4']
                remarks4 = request.form['remarks4']
                amount4 = request.form['amount4']
                user = session['username']
                from db_connect import db_conn
                conn = db_conn()
                cur = conn.cursor()
                cur.execute("insert into exit_details (exit_number,part_number,quantity,remarks,exit_by,exit_date,exit_type,amount,workshop_name) values (%s,%s,%s,%s,%s,%s,%s,%s,%s)",(exit_number,part_number4,quantity4,remarks4,user,str(datetime.datetime.now()),'WKSHOP',amount4,workshop_name))
                conn.commit()
                conn.close()
                return render_template('new_ws_exit.html',notification_msg="Item Details have been successfully entered.", item_details = list_parts_available())
            elif 'username' not in session:
                return render_template('login_page.html',msg = 'Please Login First!!')
    except Exception as e:
        print(e)
        return render_template('new_ws_exit.html', notification_msg="Entry Failed!!!", user = session['username'], item_details = list_parts_available())

@app.route('/counter_sales')
def counter_sales():
    if 'username' in session:
        return render_template('counter_sales.html',user = session['username'], item_details = list_parts_available())
    elif 'username' not in session:
        return render_template('login_page.html',msg = 'Please Login First!!')

@app.route('/counter_sales_submit', methods=['POST','GET'])
def counter_sales_submit():
    try:
        if request.method == 'POST':
            if 'username' in session:
                exit_number = 'EX_%s' % datetime.datetime.now().strftime("%Y%m%d%H%M%S")
                part_number5 = request.form['part_number5']
                part_name5 = request.form['part_name5']
                quantity5 = request.form['quantity5']
                remarks5 = request.form['remarks5']
                amount5 = request.form['amount5']
                user = session['username']
                from db_connect import db_conn
                conn = db_conn()
                cur = conn.cursor()
                cur.execute("insert into exit_details (exit_number,part_number,part_name,quantity,remarks,exit_by,exit_date,exit_type,amount) values (%s,%s,%s,%s,%s,%s,%s,%s,%s)",(exit_number,part_number5,part_name5,quantity5,remarks5,user,str(datetime.datetime.now()),'CNTRSALE',amount5))
                conn.commit()
                conn.close()
                return render_template('counter_sales.html',notification_msg="Item Details have been successfully entered.", item_details = list_parts_available())
            elif 'username' not in session:
                return render_template('login_page.html',msg = 'Please Login First!!')
    except Exception as e:
        print(e)
        return render_template('counter_sales.html', notification_msg="Entry Failed!!!", user = session['username'], item_details = list_parts_available())

@app.route('/damaged_items')
def damaged_items():
    if 'username' in session:
        return render_template('damaged_items.html',user = session['username'], item_details = list_parts_available())
    elif 'username' not in session:
        return render_template('login_page.html',msg = 'Please Login First!!')

@app.route('/damaged_items_submit', methods=['POST','GET'])
def damaged_items_submit():
    try:
        if request.method == 'POST':
            if 'username' in session:
                exit_number = 'EX_%s' % datetime.datetime.now().strftime("%Y%m%d%H%M%S")
                part_number6 = request.form['part_number6']
                part_name6 = request.form['part_name6']
                quantity6 = request.form['quantity6']
                remarks6 = request.form['remarks6']
                amount6 = request.form['amount6']
                user = session['username']
                from db_connect import db_conn
                conn = db_conn()
                cur = conn.cursor()
                cur.execute("insert into exit_details (exit_number,part_number,part_name,quantity,remarks,exit_by,exit_date,exit_type,amount) values (%s,%s,%s,%s,%s,%s,%s,%s,%s)",(exit_number,part_number6,part_name6,quantity6,remarks6,user,str(datetime.datetime.now()),'DAMAGED',amount6))
                conn.commit()
                conn.close()
                return render_template('damaged_items.html',notification_msg="Item Details have been successfully entered.", item_details = list_parts_available())
            elif 'username' not in session:
                return render_template('login_page.html',msg = 'Please Login First!!')
    except Exception as e:
        print(e)
        return render_template('damaged_items.html', notification_msg="Entry Failed!!!", user = session['username'], item_details = list_parts_available())

@app.route('/items_lookup', methods=['POST','GET'])
def items_lookup():
    try:
        if request.method == 'GET':
            if 'username' in session:
                conn = db_conn()
                cur = conn.cursor()
                qry = "select * from item_details_table where part_number in (select distinct(part_number) from entry_details)"
                cur.execute(qry)
                result10 = (cur.fetchall())
                conn.close()
                return render_template('items_lookup.html', items_lookup=result10, user = session['username'])
            elif 'username' not in session:
                return render_template('login_page.html', msg='Please Login First!!', user = session['username'])
    except Exception as e:
        print(e)
        return render_template('items_lookup.html',user = session['username'])


@app.route('/logout')
def log_out():
    session.pop('username', None)
    return render_template('login_page.html')

if  __name__ == "__main__":
    app.run()
