def manual_entry_handler(user,request_form):
    import datetime
    from db_connect import db_conn
    conn = db_conn()
    print(request_form)
    i = 2
    entry_number = 'ENTRY_%s' % datetime.datetime.now().strftime("%Y%m%d%H%M%S")
    bill_number = request_form['bill_number1']
    bill_date = request_form['bill_date1']
    remarks = request_form['remarks1']
    purchased_from = request_form['purchased_from1']
    if bill_number == "" or bill_date == "" or purchased_from == "":
        return('EMPTY_FIELDS')

    print(bill_number)
    print(entry_number)
    print(user)
    print(bill_date)
    print(remarks)
    print(purchased_from)
    print(len(request_form))
    print((len(request_form)-4)/4)
    while i < (len(request_form)-4)/4 + 2:
        part_number = 'part_number' + str(i)
        quantity = 'quantity' + str(i)
        amount = 'amount' + str(i)
        if (part_number) in request_form:
            if request_form[part_number] == "" or request_form[quantity] == "" or request_form[amount] == "":
                return('EMPTY_FIELDS')
            i += 1
            cur = conn.cursor()
            qry = "insert into entry_details (entry_number,bill_number,purchased_from,part_number,quantity,entered_by,entered_date,entry_type,amount,remarks) values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(entry_number,bill_number,purchased_from,request_form[part_number], request_form[quantity],user,str(datetime.datetime.now()), 'MANUAL', request_form[amount],remarks)
            print(qry)
            cur.execute("insert into entry_details (entry_number,bill_number,purchased_from,part_number,quantity,entered_by,entered_date,entry_type,amount,remarks,bill_date) values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(entry_number,bill_number,purchased_from,request_form[part_number], request_form[quantity],user,str(datetime.datetime.now()), 'MANUAL', request_form[amount],remarks,bill_date))
    conn.commit()
    conn.close()
    return('SUCCESS')

