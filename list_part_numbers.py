def list_parts():
    try:
        from db_connect import db_conn
        conn = db_conn()
        cur = conn.cursor()
        cur.execute("select concat(part_number,'::',description) from item_details_table order by 1")
        res = cur.fetchall()
        conn.close()
        return(res)
    except Exception as e:
        print(e)
        return(e)

def list_parts_available():
    try:
        from db_connect import db_conn
        conn = db_conn()
        cur = conn.cursor()
        cur.execute("select concat(part_number,'::',description) from item_details_table where part_number in (select distinct(part_number) from entry_details) order by 1")
        res = cur.fetchall()
        conn.close()
        return(res)
    except Exception as e:
        print(e)
        return(e)

