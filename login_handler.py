from flask import request, Flask, render_template, session, redirect, jsonify
import datetime
import hashlib
#from config_file import *
from db_connect import db_conn
from list_part_numbers import list_parts,list_parts_available
from user_audit import user_audit
from dashboard_display import dashboard_display
app = Flask(__name__)
app.secret_key = 'nchl'

@app.route('/', methods = ['POST','GET'])
def root_page():
    if request :
        print(request)
        try:
            if 'username' not in session:
                return render_template('login_page.html')
            elif 'username' and 'org_id' in session:
                user_audit(org_id=session['org_id'], username=session['username'],table_name='NO_TABLE',action='GET_ROOT_PAGE',request_items=request)
                return render_template('dashboard.html',user = session['username'],cards = dashboard_display(org_id=session['org_id']))
            else:
                user_audit(org_id=session['org_id'], username=session['username'],table_name='NO_TABLE',action='TRY_LOGOUT',request_items=request)
                return redirect('/logout')
        except Exception as e:
            print('Error Occured',e)
            return render_template('login_page.html',msg = 'Something Happened! Please Contact Administrator!')


@app.route('/login', methods = ['POST'])
def homepage():
    try:
        if 'username' and 'org_id' not in session:
            try:
                if request.form['username'] is not None:
                    username = request.form['username']
                    password = request.form['password']
                    conn = db_conn()
                    cur = conn.cursor()
                    qry = "select password,org_id from user_table where username = (%s)"
                    print(qry,(username,))

                    cur.execute(qry,(username,))
                    details = cur.fetchone()
                    print(details)
                    password_e=details[0]
                    print(hashlib.sha256(password.encode()).hexdigest())
                    print(password_e)
                    print(username)
                    org_id = details[1]
                    if hashlib.sha256(password.encode()).hexdigest() == password_e:
                        session['username'] = username
                        session['org_id'] = org_id
                        user_audit(org_id,username,'session_table','LOGIN',request)
                        return render_template('main_page.html', user=username, org_id = org_id)
                    else:
                        return render_template('login_page.html',msg = "Login Failed. Please try again!!")
            except Exception as e:
                print(e)
                return render_template('login_page.html', msg="Login Failed with Exception. Please try again!!")
        elif 'username' and 'org_id' in session:
            username = session['username']
            org_id = session['org_id']
            return render_template('main_page.html', user = username, org_id = org_id)
    except Exception as e:
        print(e)
        return render_template('login_page.html', msg="Something unexpected happened. Please try again !!")

@app.route('/dashboard')
def dashboard():
    if 'username' and 'org_id' in session:
        from dashboard_display import dashboard_display
        return render_template('dashboard.html',user = session['username'], org_id = session['org_id'],cards = dashboard_display(session['org_id']))
    elif 'username' and 'org_id' not in session:
        return render_template('login_page.html',msg = 'Please Login First!!')

@app.route('/manual_entry')
def manual_entry():
    if 'username' and 'org_id' in session:
        print(list_parts())
        return render_template('manual_entry.html',user = session['username'], org_id = session['org_id'], item_details = list_parts())
    elif 'username' and 'org_id' not in session:
        return render_template('login_page.html',msg = 'Please Login First!!')


@app.route('/manual_entry_submit', methods = ['POST','GET'])
def manual_entry_submit():
    try:
        if request.method == 'POST':
            if 'username' and 'org_id' in session:
                from post_values_handler import items_entry_handler
                res = items_entry_handler(session['username'],session['org_id'],request.form,'MANUAL')
                if res == 'EMPTY_FIELDS':
                    return render_template('manual_entry.html',
                                                   notification_msg="Empty Fields not allowed.",user=session['username'],org_id = session['org_id'], item_details = list_parts())
                elif res == 'SUCCESS':
                    return render_template('manual_entry.html', notification_msg="Item details Entered Successfully.",
                                       user=session['username'],org_id = session['org_id'], item_details = list_parts())
            elif 'username' and 'org_id' not in session:
                return render_template('login_page.html',msg = 'Please Login First!!')
    except Exception as e:
        print(e)
        return render_template('manual_entry.html', notification_msg="Entry Failed!! Exception Occured !!",
                               user=session['username'],org_id = session['org_id'], item_details=list_parts())


@app.route('/csv_upload')
def csv_upload():
    if 'username' and 'org_id' in session:
        return render_template('csv_upload.html',user = session['username'],org_id = session['org_id'])
    elif 'username' and 'org_id' not in session:
        return render_template('login_page.html',msg = 'Please Login First!!')

@app.route('/csv_file_submit', methods = ['POST','GET'])
def csv_file_submit():
    try:
        if request.method == 'POST':
            if 'username' and 'org_id' in session:
                from post_values_handler import items_entry_handler
                res = items_entry_handler(session['username'],session['org_id'],request.form,'CSV')
                if res == 'EMPTY_FIELDS':
                    return render_template('manual_entry.html',
                                                   notification_msg="Empty Fields not allowed.",user=session['username'],org_id = session['org_id'], item_details = list_parts())
                elif res == 'SUCCESS':
                    return render_template('manual_entry.html', notification_msg="Item details Entered Successfully.",
                                       user=session['username'],org_id = session['org_id'], item_details = list_parts())
            elif 'username' and 'org_id' not in session:
                return render_template('login_page.html',msg = 'Please Login First!!')
    except Exception as e:
        print(e)
        return render_template('manual_entry.html', notification_msg="Entry Failed!! Exception Occured !!",
                               user=session['username'],org_id = session['org_id'], item_details=list_parts())
@app.route('/entry_report')
def entry_report():
    if 'username' and 'org_id' in session:
        return render_template('entry_report.html',user = session['username'],org_id = session['org_id'])
    elif 'username' not in session:
        return render_template('login_page.html',msg = 'Please Login First!!')

@app.route('/entry_report_submit', methods = ['POST','GET'])
def entry_report_submit():
    try:
        if request.method == 'POST':
            if 'username' and 'org_id' in session:
                entered_date_from = (request.form['entered_date_from']) if (request.form['entered_date_from']) else "1111-11-11"
                entered_date_to = (request.form['entered_date_to']) if (request.form['entered_date_to']) else "3000-12-12"
                bill_date3 = (request.form['bill_date3']) if (request.form['bill_date3']) else "%"
                bill_number3 = (request.form['bill_number3']) if (request.form['bill_number3']) else "%"
                entered_by = (request.form['entered_by']) if (request.form['entered_by']) else "%"
                conn = db_conn()
                cur = conn.cursor()
                qry = "select * from v_entry_report where cast(entered_date as date) between (%s) and (%s) and cast(bill_date as text) like (%s) and bill_number like (%s) and entered_by like (%s)"
                print(qry,(entered_date_from,entered_date_to,bill_date3,bill_number3,entered_by))
                cur.execute(qry,(entered_date_from,entered_date_to,bill_date3,bill_number3,entered_by))
                result = (cur.fetchall())
                conn.close()
                return render_template('entry_report.html',entry_report=result)
            elif 'username' and 'org_id' not in session:
                return render_template('login_page.html',msg = 'Please Login First!!',user = session['username'],org_id = session['org_id'])
    except Exception as e:
        print(e)
        return render_template('entry_report.html', user=session['username'],org_id = session['org_id'])

@app.route('/exit_report')
def exit_report():
    if 'username' and 'org_id' in session:
        return render_template('exit_report.html',user = session['username'],org_id = session['org_id'])
    elif 'username' not in session:
        return render_template('login_page.html',msg = 'Please Login First!!')

@app.route('/exit_report_submit', methods = ['POST','GET'])
def exit_report_submit():
    try:
        if request.method == 'POST':
            if 'username' and 'org_id' in session:
                print(request.form)
                exit_date_from = (request.form['exit_date_from']) if (request.form['exit_date_from']) else "1111-11-11"
                exit_date_to = (request.form['exit_date_to']) if (request.form['exit_date_to']) else "3000-12-12"
                bill_date3 = (request.form['bill_date3']) if (request.form['bill_date3']) else "%"
                bill_number3 = (request.form['bill_number3']) if (request.form['bill_number3']) else "%"
                exit_by = (request.form['exit_by']) if (request.form['exit_by']) else "%"
                print(exit_date_from)
                print(exit_date_to)
                print(bill_date3)
                print(bill_number3)
                print(exit_by)
                conn = db_conn()
                cur = conn.cursor()
                qry = "select * from v_exit_report where cast(exit_date as date) between (%s) and (%s) and cast(bill_date as text) like (%s) and bill_number like (%s) and exit_by like (%s)"
                print(qry,(exit_date_from,exit_date_to,bill_date3,bill_number3,exit_by))
                cur.execute(qry,(exit_date_from,exit_date_to,bill_date3,bill_number3,exit_by))
                result = (cur.fetchall())
                conn.close()
                return render_template('exit_report.html',exit_report=result,user = session['username'],org_id = session['org_id'])
            elif 'username' and 'org_id' not in session:
                return render_template('login_page.html',msg = 'Please Login First!!')
    except Exception as e:
        print(e)
        return render_template('exit_report.html',user = session['username'],org_id = session['org_id'])


@app.route('/total_inventory')
def total_inventory():
    if 'username' and 'org_id' in session:
        return render_template('total_inventory.html',user = session['username'],org_id = session['org_id'], item_details=list_parts_available())
    elif 'username' not in session:
        return render_template('login_page.html',msg = 'Please Login First!!')

@app.route('/total_inventory_submit', methods= ['POST','GET'])
def total_inventory_submit():
    try:
        if request.method == 'POST':
            if 'username' and 'org_id' in session:
                part_number8 = request.form['part_number8'][0:12]
                part_number88 = part_number8 if part_number8 else '%'
                print(part_number88)
                conn = db_conn()
                cur = conn.cursor()
                cur.execute("select concat(entry_part_number,':::',(select description from item_details_table A where A.part_number=B.entry_part_number)),sum(entry_quantity) as enq,sum(entry_amount)ena,sum(exit_quantity)exq,sum(exit_amount)exa,sum(entry_quantity)-sum(exit_quantity) as avq, sum(entry_amount)-sum(exit_amount) as ava from v_inventory B where entry_part_number like %(part_number88)s and entry_org_id = '0001' group by entry_part_number",{'part_number88': str(part_number88)})
                print("select concat(entry_part_number,':::',(select description from item_details_table A"
                            " where A.entry_part_number=B.entry_part_number)),sum(entry_quantity) as enq,"
                            "sum(entry_amount)ena,sum(exit_quantity)exq,sum(exit_amount)exa,"
                            "sum(entry_quantity)-sum(exit_quantity) as avq, sum(entry_amount)-sum(exit_amount) as ava "
                            "from v_inventory B where entry_part_number like %(part_number88)s and entry_org_id = '0001' "
                            "group by entry_part_number",{'part_number88': str(part_number88)})
                result88 = (cur.fetchall())
                print('checck')
                conn.close()
                print(result88)
                return render_template('total_inventory.html',user = session['username'],org_id = session['org_id'] ,total_inventory = result88, item_details=list_parts_available())
            elif 'username' not in session:
                return render_template('login_page.html',msg = 'Please Login First!!')
    except Exception as e:
        return render_template('total_inventory.html', user=session['username'],org_id = session['org_id'], item_details=list_parts_available())

@app.route('/workshop_exit')
def workshop_exit():
    if 'username' and 'org_id' in session:
        from item_details import pending_ws_items
        pending_ws_items = pending_ws_items(session['org_id'])
        return render_template('pending_ws_exit.html',user = session['username'],org_id = session['org_id'], pending_ws_items = pending_ws_items)
    elif 'username' not in session:
        return render_template('login_page.html',msg = 'Please Login First!!')

@app.route('/workshop_exit_new')
def workshop_exit_new():
    if 'username' and 'org_id' in session:
        return render_template('new_ws_exit.html',user = session['username'],org_id = session['org_id'], item_details = list_parts_available())
    elif 'username' not in session:
        return render_template('login_page.html',msg = 'Please Login First!!')

@app.route('/p_workshop_exit_form_submit', methods=['POST','GET'])
def p_workshop_exit_form_submit():
    try:
        if request.method == 'POST':
            if 'username' and 'org_id' in session:
                exit_type = 'PWS'
                from post_values_handler import workshop_exit_handler
                res = workshop_exit_handler(session['username'],session['org_id'], request.form,exit_type)
                if res == 'EMPTY_FIELDS':
                    return render_template('new_ws_exit.html',
                                           notification_msg="Empty Fields not allowed.", user=session['username'],org_id = session['org_id'], item_details=list_parts_available())
                elif res == 'SUCCESS':
                    return render_template('new_ws_exit.html', notification_msg="JC Added Successfully.",
                                           user=session['username'],org_id = session['org_id'], item_details=list_parts_available())
            elif 'username' not in session:
                return render_template('login_page.html', msg='Please Login First!!')
    except Exception as e:
            print(e)
            return render_template('new_ws_exit.html', notification_msg="Exit Failed!! Exception Occured !!",
                                   user=session['username'],org_id = session['org_id'], item_details=list_parts_available())


@app.route('/counter_sales')
def counter_sales():
    if 'username' and 'org_id' in session:
        return render_template('counter_sales.html',user = session['username'],org_id = session['org_id'], item_details = list_parts_available())
    elif 'username' not in session:
        return render_template('login_page.html',msg = 'Please Login First!!')

@app.route('/counter_sales_submit', methods=['POST','GET'])
def counter_sales_submit():
    try:
        if request.method == 'POST':
            if 'username' and 'org_id' in session:
                exit_type = 'CS'
                from post_values_handler import workshop_exit_handler
                res = workshop_exit_handler(session['username'],session['org_id'], request.form,exit_type)
                if res == 'EMPTY_FIELDS':
                    return render_template('counter_sales.html',
                                           notification_msg="Empty Fields not allowed.", user=session['username'],org_id = session['org_id'],
                                           item_details=list_parts_available())
                elif res == 'SUCCESS':
                    return render_template('counter_sales.html', notification_msg="Items Exited Successfully.",
                                           user=session['username'],org_id = session['org_id'], item_details=list_parts_available())
            elif 'username' not in session:
                return render_template('login_page.html', msg='Please Login First!!')
    except Exception as e:
            print(e)
            return render_template('counter_sales.html', notification_msg="Exit Failed!! Exception Occured !!",
                                   user=session['username'],org_id = session['org_id'], item_details=list_parts_available())

@app.route('/damaged_items')
def damaged_items():
    if 'username' and 'org_id' in session:
        return render_template('damaged_items.html',user = session['username'], org_id = session['org_id'], item_details = list_parts_available())
    elif 'username' not in session:
        return render_template('login_page.html',msg = 'Please Login First!!')

@app.route('/damaged_items_submit', methods=['POST','GET'])
def damaged_items_submit():
    try:
        if request.method == 'POST':
            if 'username' and 'org_id' in session:
                exit_type = 'DI'
                from post_values_handler import workshop_exit_handler
                res = workshop_exit_handler(session['username'],session['org_id'], request.form,exit_type)
                if res == 'EMPTY_FIELDS':
                    return render_template('damaged_items.html',
                                           notification_msg="Empty Fields not allowed.", user=session['username'],org_id = session['org_id'],
                                           item_details=list_parts_available())
                elif res == 'SUCCESS':
                    return render_template('damaged_items.html', notification_msg="Items Exited Successfully.",
                                           user=session['username'],org_id = session['org_id'], item_details=list_parts_available())
            elif 'username' not in session:
                return render_template('login_page.html', msg='Please Login First!!')
    except Exception as e:
            print(e)
            return render_template('damaged_items.html', notification_msg="Exit Failed!! Exception Occured !!",
                                   user=session['username'],org_id = session['org_id'], item_details=list_parts_available())

@app.route('/items_lookup', methods=['POST','GET'])
def items_lookup():
    try:
        if request.method == 'GET':
            if 'username' and 'org_id' in session:
                conn = db_conn()
                cur = conn.cursor()
                qry = "select * from item_details_table where part_number in (select distinct(part_number) from entry_details)"
                cur.execute(qry)
                result10 = (cur.fetchall())
                conn.close()
                return render_template('items_lookup.html', items_lookup=result10, user = session['username'])
            elif 'username' not in session:
                return render_template('login_page.html', msg='Please Login First!!', user = session['username'],org_id = session['org_id'])
    except Exception as e:
        print(e)
        return render_template('items_lookup.html',user = session['username'],org_id = session['org_id'])

@app.route('/wishlist')
def wishlist():
    try:
        if 'username' and 'org_id' in session:
                conn = db_conn()
                cur = conn.cursor()
                qry = "select part_number,count(*) from wishlist_table group by part_number order by 2"
                cur.execute(qry)
                result = (cur.fetchall())
                print(result)
                conn.close()
                return render_template('wishlist.html', result11=result, user = session['username'])
        else:
            return render_template('login_page.html', msg='Please Login First!!', user=session['username'],
                                   org_id=session['org_id'])
    except Exception as e:
        print('Error:',e)
        return render_template('wishlist.html',user = session['username'],org_id = session['org_id'],msg="Error! Contact Admin")

@app.route('/modal_display_entry_reports')
def modal_display_entry_reports():
        try:
            entrynumber = request.args.get('entrynumber',0,type=str)
            print(entrynumber)
            conn = db_conn()
            cur = conn.cursor()
            cur.execute("select part_number,part_name,cast(quantity as text),cast(amount as text),bill_date,entered_by,remarks,purchased_from from entry_details where entry_number=%(entrynumber)s",{'entrynumber':str(entrynumber)})
            result = (cur.fetchall())
            print((result))
            conn.close()
            return jsonify(modal_results = result)
        except Exception as e:
            return jsonify(modal_results = str(e))

@app.route('/modal_display_exit_reports')
def modal_display_exit_reports():
        try:
            exitnumber = request.args.get('exitnumber',0,type=str)
            print(exitnumber)
            conn = db_conn()
            cur = conn.cursor()
            cur.execute("select part_number,part_name,cast(quantity as text),cast(amount as text),bill_date,exit_by,remarks,sold_to from exit_details where exit_number=%(exitnumber)s",{'exitnumber':str(exitnumber)})
            result = (cur.fetchall())
            print((result))
            conn.close()
            return jsonify(modal_results = result)
        except Exception as e:
            return jsonify(modal_results = str(e))

@app.route('/modal_display_inventory_reports')
def modal_display_inventory_reports():
        try:
            part_number_inv = request.args.get('part_number_inv',0,type=str)
            print(part_number_inv)
            conn = db_conn()
            cur = conn.cursor()
            cur.execute("select part_number,part_name,cast(quantity as text),remarks,entered_by,left(entered_date,10),entry_number,"
                        "entry_type,cast(amount as text),cast(bill_date as text),bill_number,purchased_from from entry_details where part_number=%(partnumberinv)s order by id desc",{'partnumberinv':str(part_number_inv)})
            result1 = (cur.fetchall())
            cur.execute("select part_number,part_name,cast(quantity as text),remarks,exit_by,left(exit_date,10),exit_number,"
                        "exit_type,cast(amount as text),cast(bill_date as text),bill_number,sold_to from exit_details where part_number=%(partnumberinv)s order by id desc",{'partnumberinv':str(part_number_inv)})
            result2 = (cur.fetchall())
            print((result1))
            print(result2)
            conn.close()
            return jsonify(modal_results1 = result1, modal_results2 = result2)
        except Exception as e:
            return jsonify(modal_results = str(e))

@app.route('/addto_wishlist')
def addto_wishlist():
    try:
        part_number = request.args.get('part_number', 0, type=str)
        print(part_number)
        conn = db_conn()
        cur = conn.cursor()
        cur.execute("insert into wishlist_table (part_number) values (%(part_number)s)",{'part_number':part_number})
        conn.commit()
        conn.close()
        return jsonify(result = 'Item Added to Wishlist')
    except Exception as e:
        print(e)
        return jsonify(result = str(e))

@app.route('/removefrom_wishlist')
def removefrom_wishlist():
    try:
        if 'username' and 'org_id' in session:
            if request.method == 'GET':
                part_number_remove = request.args.get('part_number_remove')
                print(part_number_remove)
                conn = db_conn()
                cur = conn.cursor()
                cur.execute("delete from wishlist_table where part_number = %(part_number_remove)s",
                            {'part_number_remove': str(part_number_remove)})
                print('--')
                print(part_number_remove)
                conn.commit()
                qry = "select part_number,count(*) from wishlist_table group by part_number order by 2"
                cur.execute(qry)
                result = (cur.fetchall())
                print(result)
                conn.close()
                return render_template('wishlist.html', result11=result, user = session['username'],msg="Item Removed.")
        else:
            return render_template('login_page.html', msg='Please Login First!!', user=session['username'],
                                   org_id=session['org_id'])

    except Exception as e:
        print('Error',e)
        return render_template('wishlist.html',msg = 'Exception!')


@app.route('/logout')
def log_out():
    session.pop('username', None)
    session.pop('org_id', None)
    return render_template('login_page.html')

if  __name__ == "__main__":
    app.run(host = '0.0.0.0', port='8080')
