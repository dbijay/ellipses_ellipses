def items_entry_handler(user,org_id,request_form,entry_type):
    import datetime
    from db_connect import db_conn
    conn = db_conn()
    print(request_form)
    i = 2
    j = i
    entry_number = 'EN_%s' % datetime.datetime.now().strftime("%y%m%d%H%M%S")
    bill_number = request_form['bill_number1']
    bill_date = request_form['bill_date1']
    remarks = request_form['remarks1']
    purchased_from = request_form['purchased_from1']
    if bill_number == "" or bill_date == "" or purchased_from == "":
        return('EMPTY_FIELDS')

    print(bill_number)
    print(entry_number)
    print(user)
    print(bill_date)
    print(remarks)
    print(purchased_from)
    print(len(request_form))
    print((len(request_form)-4)/4)
    while i < (len(request_form)-4)/4 + 2:
        part_number = 'part_number' + str(j)
        quantity = 'quantity' + str(j)
        amount = 'amount' + str(j)
        if (part_number) in request_form:
            if request_form[part_number] == "" or request_form[quantity] == "" or request_form[amount] == "":
                return('EMPTY_FIELDS')

            cur = conn.cursor()
            qry = "insert into entry_details (entry_number,bill_number,purchased_from,part_number,quantity,entered_by,entered_date,entry_type,amount,remarks,bill_date,org_id) values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(entry_number,bill_number,purchased_from,request_form[part_number], request_form[quantity],user,str(datetime.datetime.now()), 'MANUAL', request_form[amount],remarks,bill_date,org_id)
            print(qry)
            cur.execute("insert into entry_details (entry_number,bill_number,purchased_from,part_number,quantity,entered_by,entered_date,entry_type,amount,remarks,bill_date,org_id) values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(entry_number,bill_number,purchased_from,request_form[part_number], request_form[quantity],user,str(datetime.datetime.now()), entry_type, request_form[amount],remarks,bill_date,org_id))
            i += 1
        j += 1
    conn.commit()
    conn.close()
    return('SUCCESS')


def workshop_exit_handler(user,org_id,request_form,exit_type):
    import datetime
    from db_connect import db_conn
    conn = db_conn()
    print(request_form)
    print(len(request_form))
    i = 2
    j = i
    exit_number = 'EX_%s' % datetime.datetime.now().strftime("%y%m%d%H%M%S")
    if exit_type == 'PWS':
        jc_number = request_form['jc_number']
        print(jc_number)
        bike_number = request_form['bike_number']
        print(bike_number)
        jc_date = request_form['jc_date']
        print(jc_date)
        bike_model = request_form['bike_model']
        print(bike_model)
        bike_version = request_form['bike_version']
        print(bike_version)
        ws_type = request_form['ws_type']
        print(ws_type)
        #remarks = request_form['remarks1']
        print(jc_number)#,bike_number,jc_date,bike_model,bike_version,ws_type,remarks)
        if jc_number == "" or bike_number == "" or jc_date == "" or ws_type == "":
            return('EMPTY_FIELDS')
        print(jc_number)
        print((jc_date))
        print(bike_number)
        print(ws_type)
        while i < (len(request_form) - 6) / 4 + 2:

            part_number = 'part_number' + str(j)
            quantity = 'quantity' + str(j)
            amount = 'amount' + str(j)
            print(part_number)
            print(quantity)
            print(amount)
            if (part_number) in request_form:
                if request_form[part_number] == "" or request_form[quantity] == "" or request_form[amount] == "":
                    return ('EMPTY_FIELDS')
                cur = conn.cursor()
                qry1=cur.execute("INSERT INTO public.pending_ws_table(org_id, jc_number, jc_date, bike_number, bike_model, bike_version, ws_type, entered_by, entered_date, bill_number, status)	VALUES ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}')".format(org_id,jc_number,jc_date,bike_number,bike_model,bike_version,ws_type,user,datetime.datetime.now().strftime("%y%m%d%H%M%S"),jc_number,'NEW'))
                print('qry1')
                qry2 = cur.execute("insert into exit_details (exit_number,bill_number,bill_date,sold_to,part_number,quantity,exit_by,exit_date,exit_type,amount,remarks,org_id) values ('{0}','{1}','{2}','{3}','{4}',{5},'{6}','{7}','{8}',{9},'{10}','{11}')".format(exit_number,jc_number,jc_date,bike_number,request_form[part_number], request_form[quantity],user,str(datetime.datetime.now()), exit_type, request_form[amount],ws_type,org_id))
                print(qry1)
                print(qry2)
                i += 1
            j += 1
        conn.commit()
        conn.close()
        return ('SUCCESS')


    elif exit_type == 'CS':
        bill_number = request_form['bill_number1']
        bill_date = request_form['bill_date1']
        remarks = request_form['remarks1']
        sold_to = request_form['sold_to']
        if bill_number == "" or bill_date == "" or sold_to == "":
            return('EMPTY_FIELDS')
        print(exit_type)
        print(bill_number)
        print(exit_number)
        print(user)
        print(bill_date)
        print(remarks)
        print(sold_to)
        print(len(request_form))
        print((len(request_form)-4)/4)
        while i < (len(request_form)-4)/4 + 2:

            part_number = 'part_number' + str(j)
            quantity = 'quantity' + str(j)
            amount = 'amount' + str(j)
            print(part_number)
            print(quantity)
            print(amount)
            if (part_number) in request_form:
                if request_form[part_number] == "" or request_form[quantity] == "" or request_form[amount] == "":
                    return('EMPTY_FIELDS')
                cur = conn.cursor()
                qry = "insert into exit_details (exit_number,bill_number,bill_date,sold_to,part_number,quantity,exit_by,exit_date,exit_type,amount,remarks,org_id) values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(exit_number,bill_number,bill_date,sold_to,request_form[part_number], request_form[quantity],user,str(datetime.datetime.now()), exit_type, request_form[amount],remarks,org_id)
                print(qry)
                cur.execute("insert into exit_details (exit_number,bill_number,bill_date,sold_to,part_number,quantity,exit_by,exit_date,exit_type,amount,remarks,org_id) values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(exit_number,bill_number,bill_date,sold_to,request_form[part_number], request_form[quantity],user,str(datetime.datetime.now()), exit_type, request_form[amount],remarks,org_id))
                i += 1
            j += 1

        conn.commit()
        conn.close()
        return('SUCCESS')

