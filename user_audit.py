def user_audit(org_id,username,table_name,action,request_items):
    try:
        from db_connect import db_conn
        import datetime
        conn = db_conn()
        cur = conn.cursor()
        print(username)
        print(table_name)
        print(action)
        print(request_items.environ['REMOTE_ADDR'])
        cur.execute("insert into user_audit_table (org_id,username,table_name,action,action_time,ipaddress) values (%s,%s,%s,%s,%s,%s)",(org_id,username,table_name,action,str(datetime.datetime.now()),request_items.environ['REMOTE_ADDR']))
        conn.commit()
        conn.close()
        return('SUCCESS')

    except Exception as e:
        print(e)
        return(e)

